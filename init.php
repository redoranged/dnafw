<?php
/**
 * Composer Library
 */

require_once 'vendor/autoload.php';

/**
 * Initialize
 */

require_once 'config.php';

session_start();

define('TITLE_NAME', $title_name ?? 'DnA Framework');
define('URL', $protocol.'://'.$url);
define('ROOT', $root_path.$app_name);

define('DB_HOST', $db_host);
define('DB_USER', $db_user);
define('DB_PASS', $db_pass);
define('DB_NAME', $db_name);

require_once 'library/DB.php';
require_once 'library/Session.php';
require_once 'library/function.php';
require_once 'library/Auth.php';

$session = new Session();
$auth = new Auth();

/**
 * Importing Models
 */

require_once 'model/User.php';
require_once 'model/Product.php';
?>