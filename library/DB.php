<?php

class DB
{
    public $mysqli = '';

    function __construct()
    {
        $this->mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        if($this->mysqli->connect_errno)
        {
            echo "Failed to connect to MySQL: ".$this->mysqli->connect_error;
            exit();
        }
    }
}
?>