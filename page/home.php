<?php
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once ROOT.'/page/components/breadcrumb.php';

?>
<div class="card center-align z-depth-2" style="margin-top: 15px;">
    <div class="card-content">
        <span class="card-title">DnA Framework</span>
    </div>
    <div class="card-action">
        <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Architecto, voluptatum cum. Delectus earum, laborum maiores sit error iure, magni tenetur pariatur nobis provident totam perferendis magnam architecto corporis blanditiis quas?
        </p>
    </div>
</div>