<?php
    $product = new Product();
    $product = $product->find($_GET['id']);

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Products',
            'link' => url('/products')
        ],
        [
            'title' => $product['name'],
            'link' => 'javascript:void(0)'
        ],
    ];

    include_once load_component('breadcrumb');


?>
<br>
<?php
    include 'view/form.php';
    
?>