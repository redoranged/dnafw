<?php
    $model_product = new Product();
    $products = $model_product->select();

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Products',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

    // Floating Button Setup
    $button_items = [
        [
            'name' => 'Export Data',
            'icon' => 'file_download',
            'class' => 'purple',
            'link' => url('/products/export')
        ],
        [
            'name' => 'Import Data',
            'icon' => 'insert_drive_file',
            'class' => 'blue modal-trigger',
            'link' => '#modal-products'
        ],
        [
            'name' => 'Input Manual',
            'icon' => 'add',
            'class' => 'green',
            'link' => url('/products/create')
        ]
    ];
    include_once load_component('floating-button');
?>
<br>
<?php
    include 'view/table.php';

    $modal = [
        'id' => 'modal-products',
        'model' => 'products',
        'text' => 'Excel Template : <a href="'.public_url('template/products-template.xlsx').'" download/>products-template.xlsx</a>',
        'action' => url('/products/import'),
    ];

    include_once load_component('modal-import');
?>    