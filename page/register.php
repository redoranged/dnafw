<div class="container">
    <div class="row">
        <div class="col s12 m6 offset-m3">
            <form id="register_form" method="post" action="<?=url('/auth/register')?>">
                <div class="card z-depth-3">
                    <div class="card-content"> 
                        <span class="card-title" style="font-weight:400;">Register</span>
                        <div class="input-field">
                            <input name="name" id="name" type="text" class="validate" required>
                            <label for="name">Name</label>
                        </div>
                        <div class="input-field">
                            <input name="username" id="username" type="text" class="validate" required>
                            <label for="username">Username</label>
                        </div>
                        <div class="input-field">
                            <select name="role" id="role" class="validate" required>
                                <option value="user" selected>User</option>
                            </select>
                            <label for="role">Role</label>
                        </div>
                        <div class="input-field">
                            <input name="password" id="password" type="password" class="validate" required>
                            <label for="password">Password</label>
                        </div>
                        <div class="input-field">
                            <input id="confirm_password" type="password" class="validate" required>
                            <label for="confirm_password">Confirm Password</label>
                            <span class="helper-text" data-error="Please enter confirm password correctly."></span>
                        </div>
                        <div class="row">
                            <div class="col sm6">
                                <a href="<?=url('/login')?>">I have an account!</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button type="submit" class="btn">Register</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        function checkConfirmPassword()
        {
            if ($("#password").val() != $('#confirm_password').val()) {
                $('#confirm_password').removeClass("valid").addClass("invalid");
                $('#button-submit').prop('disabled', true)
            } else {
                $('#confirm_password').removeClass("invalid").addClass("valid");
                $('#button-submit').prop('disabled', false)
            }
        }
        
        $("#password").on("change keyup focusout", function (e) {
            checkConfirmPassword()
        });
        $("#confirm_password").on("change keyup focusout", function (e) {
            checkConfirmPassword()
        });
    });
</script>