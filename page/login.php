<div class="container">
    <div class="row">
        <div class="col s12 m6 offset-m3">
            <form id="login_form" method="post" action="<?=url('/auth/login')?>">
                <div class="card z-depth-3">
                    <div class="card-content"> 
                        <span class="card-title" style="font-weight:400;">Sign In</span>
                        <div class="input-field">
                            <input id="username" name="username" type="text" class="validate" required>
                            <label for="username">Username</label>
                        </div>
                        <div class="input-field">
                            <input id="password" name="password" type="password" class="validate" required>
                            <label for="password">Password</label>
                        </div>
                        <div class="row">
                            <div class="col sm6">
                                <a href="<?=url("/register")?>">Don't have account?</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                    <button type="submit" class="btn">Sign In</button>
                </div>
            </div>
        </form>
    </div>
</div>