<?php
require_once 'Model.php';

class User extends Model
{
    public $name = 'User';
    public $table = 'users';
    public $primaryKey = 'user_id';
    protected $columns = ['name', 'username', 'password', 'role'];
    protected $hide = ['password'];
    protected $many2one = [];
    protected $one2many = [];

    public function login($username, $password)
    {
        $user = $this->select('WHERE username="'.$username.'"');

        if(count($user) <= 0)
        {
            $this->sessionError("User Not Found!");
            return false;
        }else{
            $user = $this->select('WHERE username="'.$username.'" AND password="'.md5($password).'"');
            if(count($user) <= 0)
            {
                $this->sessionError("Wrong Password!");
                return false;
            }else{
                return $user[0];
            }
        }
        return false;
    }

    public function checkPassword($user_id, $password)
    {
        $user = $this->select('WHERE user_id="'.$user_id.'"');

        if(count($user) <= 0)
        {
            return false;
        }else{
            $user = $this->select('WHERE user_id="'.$user_id.'" AND password="'.md5($password).'"');
            if(count($user) <= 0)
            {
                return false;
            }else{
                return $user[0];
            }
        }
        return false;
    }

    /**
     * Basic Function
     */
    
    public function create($array)
    {
        $user = $this->select('WHERE username="'.$array['username'].'"');

        if(count($user) > 0)
        {
            $this->sessionError("Username '".$array['username']."' is exists. Username must be unique.");
            return false;
        }
        $query = 'INSERT INTO '.$this->table.' SET password="'.md5($array['password']).'", '.$this->queryColumn($array, ', ', false);
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->queryColumn($array, ' AND ', null),' LIMIT 1');

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }
    public function update($id, $array)
    {
        if(isset($array['password']))
        {
            $query = 'UPDATE '.$this->table.' SET password="'.md5($array['password']).'", '.$this->queryColumn($array).' WHERE '.$this->primaryKey.'='.$id;
        }else{
            $query = 'UPDATE '.$this->table.' SET '.$this->queryColumn($array).' WHERE '.$this->primaryKey.'='.$id;
        }
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->find($id);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data;
    }
}
?>