<?php
$model = new User();

if(isset($_GET['export']))
{
    $request = getRequest();
    
    $export = $model->export();
    
    if($export)
    {
        $session->setSession('success', 'Success export User!');
    }else{
        $session->setSession('error', 'Failed export User!');
    }
}

if(isset($_POST['import']))
{
    $request = getRequest();
    
    $file = $_FILES['file_import'];

    $upload = fileUpload($file);

    if(!$upload)
    {
        $session->setSession('error', 'Failed import User!');
        echo "<script>window.location.replace('".url('/users')."')</script>";
        exit;
    }

    $import = $model->import($upload['dirname'].'/'.$upload['basename'], $upload['extension']);
    
    if(!empty($import))
    {
        $session->setSession('success', 'Success import User!');
    }
}

if(isset($_POST['store']))
{
    $request = getRequest();
    
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Success create New User!');
    }
}

if(isset($_POST['destroy']))
{
    $request = getRequest();
    if(!isset($_POST['user_id'])){
        $session->setSession('warning', 'User ID not identified!');
    }else{
        if($model->delete($_POST['user_id']))
        {
            $session->setSession('success', 'Success delete User!');
        }else{
            $session->setSession('warning', 'Failed delete User!');
        }
    }
}

if(isset($_POST['update']))
{
    $request = getRequest();
    if(!isset($_POST['user_id'])){
        $session->setSession('warning', 'User ID tidak teridentifikasi!');
    }else{
        $passwordCorrect = true;
        if(isset($request['old_password']) && trim($request['old_password']) != '')
        {
            if(!$model->checkPassword($_POST['user_id'], $request['old_password']))
            {
                $session->setSession('warning', 'Old Password not correct!');
                $passwordCorrect = false;
            }
        }
        if(trim($request['password']) == '')
        {
            unset($request['password']);
        }

        if($passwordCorrect)
        {
            $model = $model->update($_POST['user_id'], $request);
    
            if(!empty($model))
            {
                $session->setSession('success', 'Success edit User!');
            }else{
                $session->setSession('warning', 'Failed edit User!');
            }
        }
    }
}

echo "<script>window.location.replace('".url('/users')."')</script>";
exit;

?>